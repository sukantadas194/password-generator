import string
import random

def gen():
    s1 = string.ascii_uppercase
    s2 = string.ascii_lowercase
    s3 = string.digits
    s4 = string.punctuation

    try:
        passlen = (int(input("Enter password length\n")))
        print("Do you want the generated password to be in Uppercase(U)/Lowercase(L)/Digits(D)/Punctuation(P)/Mixed(M)?")
        qu = input("Ans: ")

        if qu == "U" or qu == "u":
            u = []
            u.extend(list(s1))
            random.shuffle(u)
            xu = ("".join(u[0:passlen]))
            print(xu)

        elif qu == "L" or qu == "l":
            l = []
            l.extend(list(s2))
            random.shuffle(l)
            xl = ("".join(l[0:passlen]))
            print(xl)

        elif qu == "D" or qu == "d":
            d = []
            d.extend(list(s3))
            random.shuffle(d)
            xd = ("".join(d[0:passlen]))
            print(xd)

        elif qu == "P" or qu == "p":
            p =[]
            p.extend(list(s4))
            random.shuffle(p)
            xp = ("".join(p[0:passlen]))
            print(xp)

        elif qu == "M" or qu == "m":
            m = []
            m.extend(list(s1))
            m.extend(list(s2))
            m.extend(list(s3))
            m.extend(list(s4))

            random.shuffle(m)
            mas = ("".join(m[0:passlen]))
            print(mas)

        else:
            print("Incorrect input")
            print("Try again..")
            gen()

    except:
        print("Incorrect input")
        print("Try again..")
        gen()
gen()

